import React, { Component } from 'react'
// import "../../../../css/ta.css";
class Tags extends Component {
    constructor(props) {
        super(props)
        this.state = {
             tags:[],
             nclickfilter:[],
             input:''  
        }
    }
    sendId=(filter)=>{
        const {nclickfilter}=this.state
        {(nclickfilter.find(nclickfilter=>nclickfilter===filter.title))?alert('Duplicate tag')
        :
        this.setState({
            nclickfilter:[...this.state.nclickfilter,filter.title]  
        })
        }
        this.setState({input:''})
}
    handleChange = e => {
        this.setState({
        [e.target.name]:e.target.value
        })
      };
    addTags=()=>{
        let data ={
            id:Math.round(Math.random()*1000),
            title:this.state.input
        }        
        fetch("http://127.0.0.1:8000/api/v1/tags/create/",{method:"POST",
            body:JSON.stringify(data),
            headers:{
                'Accept':'application/json',
                'Content-Type':'application/json'}
        })
        .then((response)=>response.json())
        .then((data)=>{
         this.state.tags.push(data);
         this.setState({tags:this.state.tags})
         this.setState({
            nclickfilter:[...this.state.nclickfilter,data.title]
        })
        })
    }
    removeTags=(i)=>{
        fetch("http://127.0.0.1:8000/api/v1/tags/"+i,{
            method:"DELETE",
            headers:{'Accept':'application/json',
        'Content-Type':'application/json'}
        })
       const nTag=this.state.tags.filter(tag=>tag.id!==i);
       this.setState({tags:nTag})  
    }
    componentDidMount(){
        fetch("http://127.0.0.1:8000/api/v1/tags/")
        .then(res=>res.json())
        .then((result)=>{
            this.setState({tags:result});
        },
        (error)=>this.setState({
            tags:error
        }))
    }
    remove=(yid)=>{
        console.log(this.state.nclickfilter[yid]);
        const newTag=this.state.nclickfilter.filter(nclickfilter=>nclickfilter!==this.state.nclickfilter[yid]);
       this.setState({nclickfilter:newTag}) 
       

    }




    handleKeyPress=(e)=> {
        if((['Enter',',','Tab'].includes(e.key))&& e.target.value!==',') {
            e.preventDefault();
            var input=this.state.input.trim();
            if(input){
                this.addTags();
                this.setState({input:''})
            }   
    }
}

    render() {
        const {tags}=this.state;
        const {input}=this.state;
        // const filterdata=this.state.tags.filter(t=>{
        //     return t.title[0].toString().toLowerCase().indexOf(input.toString().toLowerCase()) !== -1;
        // })
        return (
            <div>
                {/* Tag Master:
                <div>
            <div className="tags-input">
                <ul id="tags">
                    {tags.map((tag, i) => (
                        <li key={i} className="tag">
                            <span className="tag-title">{tag.title}</span>
                            <span className="tag-close-icon"
                            onClick={() => this.removeTags(tag.id)}
                            >x</span>
                        </li>
                    ))}
                </ul>
            </div>
            </div> */}
            <div>
               <input type="text" name='input' value={this.state.input}
                placeholder="Press Enter to add tags"
                onChange={this.handleChange}
                onKeyPress={this.handleKeyPress}
                 />
            </div>
            {/* <div>
                {input.length>0?
            filterdata.map(filter =>{
                return(
                    <li className="suggestiontag" key={filter.id}>  
                       <span className="tag"
                       onClick={() => this.sendId(filter)}>
                       {filter.title}</span>
                       </li>
                )
            }):''}
            </div> */}
            <div>
                Your entered Tags are:
            <div className="tags-input">
            <ul id="tags">
                {this.state.nclickfilter.map((nclickfilter,yid)=> (
                    <li key={yid} className="tag">
                        <span className="tag-title">{nclickfilter}</span>
                        <span className="tag-close-icon"
                            onClick={() => this.remove(yid)}
                            >x</span>
                            </li>
                ))}
                </ul>
                </div>
            </div>       
            </div>
        )
    }
}
export default Tags